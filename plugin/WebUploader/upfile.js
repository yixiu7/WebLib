

function newWebUploader(id) {
	
	var $ = jQuery,		
		state = 'pending',
		uploader;

	uploader = WebUploader.create({
		auto: true,
		// 不压缩image
		resize: false,
		
		// swf文件路径
		swf:   '/js/Uploader.swf',

		// 文件接收服务端。
		server: 'http://webuploader.duapp.com/server/fileupload.php',

		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick: {
				id: '#' + id ,  // '#pickfile'
				multiple: false
			},
		chunked: true,//分片上传-大文件的时候分片上传，默认false
		chunkSize: 500 * 1024,
		fileSingleSizeLimit: 1024 * 1024 * 1024,   //设定单个文件大小 1g
		duplicate: true, // 允许重复上传	
		
	});

	// 当有文件添加进来的时候
	uploader.on( 'fileQueued', function( file ) {
		$('#' + id).find('.item').remove(); 
		$('#' + id).append('<span id="' + file.id + '" class="item">' + file.name  + '&nbsp;&nbsp;<span class="state" >等待上传...</span></span>');			
	});

	// 文件上传过程中创建进度条实时显示。
	uploader.on( 'uploadProgress', function( file, percentage ) {
		var $li = $( '#'+file.id ),
			$percent = $li.find('.progress .progress-bar');

		// 避免重复创建
		if ( !$percent.length ) {
			$percent = $('<div class="progress progress-striped active">' +
			  '<div class="progress-bar" role="progressbar" style="width: 0%">' +
			  '</div>' +
			'</div>').appendTo( $li ).find('.progress-bar');
		}

		$li.find('span.state').text('上传中');

		$percent.css( 'width', percentage * 100 + '%' );
	});

	uploader.on( 'uploadSuccess', function( file ) {
		$( '#'+file.id ).find('span.state').text('已上传');
	});

	uploader.on( 'uploadError', function( file ) {
		$( '#'+file.id ).find('span.state').text('上传出错');
	});
	
	/** 验证文件格式以及文件大小 */
	 uploader.on("error", function (type) {

		 if (type == "F_EXCEED_SIZE") {
			 alert("文件大小不能超过1G");
		 } else if(type="F_DUPLICATE") {
			  alert("文件重复上传");
		 }
		 else {
			 alert("上传出错！请检查后重新上传！错误代码" + type);
		 }
	 });
		 

	uploader.on( 'uploadComplete', function( file ) {
		$( '#'+file.id ).find('.progress').fadeOut();
	});
	 
	return uploader;
}  
 

 